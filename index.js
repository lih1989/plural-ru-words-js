const plural_dict = {
    year: {
        one: 'год',
        two: 'года',
        five: 'лет'
    },
    month: {
        one: 'месяц',
        two: 'месяца',
        five: 'месяцев'
    },
    day: {
        one: 'день',
        two: 'дня',
        five: 'дней'
    },
    hour: {
        one: 'час',
        two: 'часа',
        five: 'часов'
    },
    minute: {
        one: 'минута',
        two: 'минуты',
        five: 'минут'
    },
    user: {
        one: 'пользователь',
        two: 'пользователя',
        five: 'пользователей'
    },
    ruble: {
        one: 'рубль',
        two: 'рубля',
        five: 'рублей'
    },
};
const plural = (number, type) => {
    number = Math.abs(number);
    number %= 100;
    if (number >= 5 && number <= 20) {
        return plural_dict[type].five
    }
    number %= 10;
    if (number === 1) {
        return  plural_dict[type].one
    }
    if (number >= 2 && number <= 4) {
        return  plural_dict[type].two
    }
    return plural_dict[type].five
};

export default plural